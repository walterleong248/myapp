<?php
/**
 * Created by PhpStorm.
 * User: Walter
 * Date: 1/24/18
 * Time: 3:13 PM
 */
use App\Common
?>
@extends('layouts.app')

@section('content')

    <div class="panel-body">
        {!! Form::model($member, [
        'route' => ['member.update', $member->id],
        'class' => 'form-horizontal'
        ]) !!}
        <div class="form-group row">
            {!! Form::label('member-code', 'Code', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::text('code', $member->code, [
                'id' => 'member-code',
                'class' => 'form-control',
                'maxlength' => 3,
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('member-name', 'Name', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::text('name', $member->name, [
                'id' => 'member-name',
                'class' => 'form-control',
                'maxlength' => 100,
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('member-address', 'Address', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::textarea('address', $member->address, [
                'id' => 'member-address',
                'class' => 'form-control',
                'maxlength' => 5,
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('member-postcode', 'Postcode', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::text('postcode', $member->postcode, [
                'id' => 'member-postcode',
                'class' => 'form-control',
                'maxlength' => 5,
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('member-city', 'City', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::text('city', $member->city, [
                'id' => 'member-city',
                'class' => 'form-control',
                'maxlength' => 50,
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('member-state', 'State', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::select('state', Common::$states, $member->state, [
                'class' => 'form-control',
                'placeholder' => '- Select State -',
                ]) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-offset-3 col-sm-6">
                {!! Form::button('Update', [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                ]) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    @endsection
