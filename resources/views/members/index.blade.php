<?php
/**
 * Created by PhpStorm.
 * User: Walter
 * Date: 1/24/18
 * Time: 2:15 PM
 */
use App\Common;
?>
@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @if (count($members) > 0)
            <table class="table table-striped task-table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Created</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($members as $i => $member)
                        <tr>
                            <td class="table-text">
                                <div>{{ $i+1 }}</div>
                            </td>
                            <td class="table-text">
                                <div>
                                    {!! link_to_route(
                                        'member.show',
                                        $title = $member->code,
                                        $parameters = [
                                        'id' => $member->id,
                                        ]
                                    ) !!}
                                </div>
                            </td>
                            <td class="table-text">
                                <div>{{ $member->name }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $member->city }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ Common::$states[$member->state] }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $member->created_at }}</div>
                            </td>
                            <td class="table-text">
                                <div>{!! link_to_route(
                                    'member.edit',
                                    $title = 'Edit',
                                    $parameters = [
                                        'id' => $member->id,
                                    ]
                                ) !!}</div>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            @else
            <div>
                No records found
            </div>
            @endif
    </div>
    @endsection
