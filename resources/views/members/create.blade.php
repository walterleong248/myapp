<?php
/**
 * Created by PhpStorm.
 * User: Walter
 * Date: 1/17/18
 * Time: 3:29 PM
 */
use App\Common;

?>
@extends('layouts.app')

@section('content')

    <div class="panel-body">
        {!! Form::model($member, [
        'route' => ['member.store'],
        'class' => 'form-horizontal'
        ]) !!}
        <!-- Membership No. -->
        <div class="form-group row">
            {!! Form::label('member-membership_no', 'Membership no.', [
                'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::text('code', null, [
                'id' => 'member-membership_no',
                'class' => 'form-control',
                'maxlength' => 3,
                ]) !!}
            </div>
        </div>
            {{--NRIC--}}
            <div class="form-group row">
                {!! Form::label('member-name', 'NRIC', [
                    'class' => 'control-label col-sm-3',
                    ]) !!}
                <div class="col-sm-9">
                    {!! Form::number('nric', null, [
                    'id' => 'member-nric',
                    'class' => 'form-control',
                    'maxlength' => 100,
                    ]) !!}
                </div>
            </div>
        {{--Name--}}
        <div class="form-group row">
            {!! Form::label('member-name', 'Name', [
                'class' => 'control-label col-sm-3',
                ]) !!}
            <div class="col-sm-9">
            {!! Form::text('name', null, [
            'id' => 'member-name',
            'class' => 'form-control',
            'maxlength' => 100,
            ]) !!}

            </div>
        </div>
            {{--Name--}}
            <div class="form-group row">
                {!! Form::label('member-gender', 'Gender', [
                    'class' => 'control-label col-sm-3',
                    ]) !!}
                <div class="col-sm-9">
                    @foreach(\App\Common::$genders as $key => $val)
                        {!! Form::radio('gender', $key) !!} {{$val}}
                    @endforeach
                </div>
            </div>
        {{--Address--}}
        <div class="form-group row">
            {!! Form::label('member-address', 'Address', [
            'class' => 'control-label col-sm-3',
            ]) !!}
            <div class="col-sm-9">
                {!! Form::textarea('member-address', null, [
                'id' => 'member-address',
                'class' => 'form-control',
                ])!!}
            </div>
        </div>
        {{--Postcode--}}
            <div class="form-group row">
                {!! Form::label('member-postcode','Postcode', [
                'class' => 'control-label col-sm-3',
                ]) !!}
                <div class="col-sm-9">
                    {!! Form::text('postcode', null, [
                    'id' => 'member-postcode',
                    'class' => 'form-control',
                    'maxlength' => 5,]) !!}
                </div>
            </div>
            {{--City--}}
            <div class="form-group row">
                {!! Form::label('member-city', 'City', [
                'class' => 'control-label col-sm-3',
                ]) !!}
                <div class="col-sm-9">
                    {!! Form::text('city', null, [
                    'id' => 'member-city',
                    'class' => 'form-control',
                    'maxlength' => 50,
                    ])!!}
                </div>
            </div>
            {{--State--}}
            <div class="form-group row">
                {!! Form::label('member-state', 'State', [
                'class' => 'control-label col-sm-3',
                ]) !!}
                <div class="col-sm-9">
                    {!! Form::select('state', Common::$states, null, [
                    'class' => 'form-control',
                    'placeholder' => '- Select State -'
                    ])!!}
                </div>
            </div>
            {{--Division ID--}}
            <div class="form-group row">
                {!! Form::label('member-division_id',
                'Division ID', [
                'class' => 'control-label col-sm-3',
                ]) !!}
                <div class="col-sm-9">
                    {!! Form::select('division_id',
                    \App\Division::pluck('name', 'id'),
                    null, [
                    'class' => 'form-control',
                    'placeholder' => '- Select Division Id -'
                    ])!!}
                </div>
            </div>
            {{--Submit Button--}}
            <div class="form-group row">
                <div class="col-sm-ffset-3 col-sm-6">
                    {!! Form::button('Save', [
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    ])!!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
