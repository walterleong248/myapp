<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    //
    public function create()
    {
        $member = new Member();

        return view('members.create', [
            'member' => $member,
        ]);
    }

    public function store(Request $request)
    {
        $member = new Member();
        $member->fill($request->all());
        $member->save();

        return redirect()->route('member.index');
    }

    public function index(){
        $members = Member::orderBy('name', 'asc')->get();

        return view('members.index', [
            'members' => $members
        ]);
    }

    public function show($id)
    {
        $member = Member::find($id);
        if(!$member) throw new ModelNotFoundException;

        return view('members.show', [
            'member' => $member
        ]);
    }

    public function edit($id)
    {
        $member = Division::find($id);
        if(!$member) throw new ModelNotFoundException();
//        die(var_dump($division));
        return view('members.edit', [
            'member' => $member
        ]);

    }

    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        if(!$member) throw new ModelNotFoundException();

        $member->fill($request->all());

        $member->save();

        return redirect()->route('member.index');
    }
}
