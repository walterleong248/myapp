<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    protected $fillable = [
        'membership_no',
        'nric',
        'name',
        'address',
        'postcode',
        'city',
        'state',
    ];

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

}
